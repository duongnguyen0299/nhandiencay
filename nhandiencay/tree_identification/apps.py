from django.apps import AppConfig


class TreeIdentificationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tree_identification'
