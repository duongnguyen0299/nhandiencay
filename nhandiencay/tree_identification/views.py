from django.shortcuts import render

# Create your views here.

def render_home(request):
    return render(request,"tree_identification/index.html")

def render_about(request):
    return render(request,"tree_identification/about.html")

def render_contact(request):
    return render(request,"tree_identification/contact.html")
